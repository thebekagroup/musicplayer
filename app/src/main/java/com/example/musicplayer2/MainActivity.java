package com.example.musicplayer2;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.jgabrielfreitas.core.BlurImageView;

public class MainActivity extends AppCompatActivity {

    private BlurImageView wallpaper;
    private ImageButton playImagebutton;
    private ImageButton stopImagebutton;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wallpaper = findViewById(R.id.Wallpaper);
        playImagebutton = findViewById(R.id.playImageButton);
        stopImagebutton = findViewById(R.id.stopImageButton);
        mediaPlayer = MediaPlayer.create(getApplication(), R.raw.clock);

    }

    public void Musicbrn(View view){
        switch (view.getId()) {
            case R.id.playImageButton:
                if (!mediaPlayer.isPlaying()){
                    mediaPlayer.start();
                    playImagebutton.setImageResource(R.drawable.ic_iconfinder);
                }
                else {
                    mediaPlayer.pause();
                    playImagebutton.setImageResource(R.drawable.ic_signiufication);
                }
                break;
            case R.id.stopImageButton:
                if (mediaPlayer != null) {
                    mediaPlayer.stop();
                    playImagebutton.setImageResource(R.drawable.ic_signiufication);
                    mediaPlayer = MediaPlayer.create(getApplication(), R.raw.clock);
                }
        }
    }
}